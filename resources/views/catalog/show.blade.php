@extends('layouts.master')

@section('content')

    <div class="row">

            <div class="col-sm-4">
        
                <img class="img-responsive" src="{{$pelicula->poster}}"/>
        
            </div>
            <div class="col-sm-8">
        
                <p>
                    <h3>{{$pelicula->title}}</h3>
                    <h6>Año: {{$pelicula->year}}</h6>
                    <h6>Director: {{$pelicula->director}}</h6>
                </p>

                <p>
                    <b>Resumen: </b> {{$pelicula->synopsis}}
                </p>

                @if( $pelicula->rented === 1 )
                    <p>
                        <b>Estado: </b> Pelicula actualmente alquilada
                    </p>
                    <p>
                        <form action="{{action('CatalogController@putReturn', $pelicula->id)}}" 
                            method="POST" style="display:inline">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger" style="display:inline">
                                Devolver película
                            </button>
                        </form>
                @else
                    <p>
                        <b>Estado: </b> Pelicula disponible 
                    </p>

                    <p>
                        <form action="{{action('CatalogController@putRent', $pelicula->id)}}" 
                            method="POST" style="display:inline">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary" style="display:inline">
                                Alquilar película
                            </button>
                        </form>
                @endif

                    <a class="btn btn-default btn-warning" role="button" href="{{ action('CatalogController@getEdit', ['id' => $pelicula->id]) }}">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                        Editar película
                    </a>
                    
                    <form action="{{action('CatalogController@deleteMovie', $pelicula->id)}}" 
                        method="POST" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger" style="display:inline">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                        Eliminar película
                        </button>
                    </form>

                    <a type="button" class="btn btn-default" role="button" href="{{ action('CatalogController@getIndex') }}">
                        <i class="fa fa-chevron-left"></i>
                        Volver al listado
                    </a>

                </p>
        
            </div>
        </div>

@stop