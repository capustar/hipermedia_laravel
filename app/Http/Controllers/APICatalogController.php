<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;

class APICatalogController extends Controller
{

// -----------------------------------------------------------------------------
    
    public function index()
    {
        return response()->json( Movie::all() );
    }

// -----------------------------------------------------------------------------
    
    public function show($id)
    {
        $movie = Movie::findOrFail($id);
        return response()->json($movie);
    }
    
// -----------------------------------------------------------------------------

    public function store(Request $request)
    {
        $movie = new Movie;
        $movie->title = $request->input('title');
        $movie->year = $request->input('year');
        $movie->director = $request->input('director');
        $movie->poster = $request->input('poster');
        $movie->synopsis = $request->input('synopsis');

        $movie->save();

        return response()->json( [
                                    'error' => false,
                                    'msg' => 'La película se ha creado correctamente'
                                ] );
    }

// -----------------------------------------------------------------------------

    public function update(Request $request, $id)
    {
        $movie = Movie::findOrFail($id);

        if ($request->input('title') != null)
        {
            $movie->title = $request->input('title');
        }
        
        if ($request->input('year') != null)
        {
            $movie->year = $request->input('year');
        }

        if ($request->input('director') != null)
        {
            $movie->director = $request->input('director');
        }

        if ($request->input('poster') != null)
        {
            $movie->poster = $request->input('poster');
        }

        if ($request->input('synopsis') != null)
        {
            $movie->synopsis = $request->input('synopsis');
        }

        $movie->save();

        return response()->json( [
                                    'error' => false,
                                    'msg' => 'La película se ha editado correctamente'
                                ] );
    }    
    
// -----------------------------------------------------------------------------

    public function destroy($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->delete();

        return response()->json( [
                                    'error' => false,
                                    'msg' => 'La película se ha borrado correctamente'
                                ] );
    }

// -----------------------------------------------------------------------------

    public function putRent($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->rented = true;

        $movie->save();

        return response()->json( [
                                    'error' => false,
                                    'msg' => 'La película se ha alquilado correctamente'
                                ] );
    }
    
// -----------------------------------------------------------------------------

    public function putReturn($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->rented = false;

        $movie->save();

        return response()->json( [
                                    'error' => false,
                                    'msg' => 'La película se ha devuelto correctamente'
                                ] );       
    }
}
