<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use DB;
use Notification;

class CatalogController extends Controller
{

// -----------------------------------------------------------------------------

    public function getIndex()
    {
		$movies = DB::table('movies')->get();

        return view('catalog.index',  array('arrayPeliculas'=>$movies));
    }

// -----------------------------------------------------------------------------

    public function getShow($id)
    {
		$movie = Movie::findOrFail($id);
        return view('catalog.show', array('pelicula'=>$movie, 'id'=> $id));
    }

// -----------------------------------------------------------------------------

    public function getCreate()
    {
        return view('catalog.create');
    }

// -----------------------------------------------------------------------------
    
    public function postCreate(Request $request)
    {
        $movie = new Movie;
        $movie->title = $request->input('title');
        $movie->year = $request->input('year');
        $movie->director = $request->input('director');
        $movie->poster = $request->input('poster');
        $movie->synopsis = $request->input('synopsis');

        $movie->save();

        Notification::success('La película se ha guardado correctamente');
        
        return redirect('/catalog');
    }

// -----------------------------------------------------------------------------

    public function getEdit($id)
    {
		$movie = Movie::findOrFail($id);
        return view('catalog.edit', array('pelicula'=>$movie));
    }

// -----------------------------------------------------------------------------

    public function putEdit($id, Request $request)
    {
		$movie = Movie::findOrFail($id);
        $movie->title = $request->input('title');
        $movie->year = $request->input('year');
        $movie->director = $request->input('director');
        $movie->poster = $request->input('poster');
        $movie->synopsis = $request->input('synopsis');

        $movie->save();

        Notification::success('La película se ha modificado correctamente');

        return redirect()->action('CatalogController@getShow', $id);
    }

// -----------------------------------------------------------------------------

    public function putRent($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->rented = true;

        $movie->save();

        Notification::success('La película se ha alquilado correctamente');

        return redirect()->action('CatalogController@getShow', $id);
    }
    
// -----------------------------------------------------------------------------

    public function putReturn($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->rented = false;

        $movie->save();

        Notification::success('La película se ha devuelto correctamente');

        return redirect()->action('CatalogController@getShow', $id);        
    }
    
// -----------------------------------------------------------------------------

    public function deleteMovie($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->delete();

        Notification::success('La película se ha borrado correctamente');
        return redirect('/catalog');
    }

}
